import mill._
import mill.scalalib._

object diceNDragons extends ScalaModule {

  def scalaVersion = "3.2.2"

  def ivyDeps = Agg(
      ivy"dev.zio::zio-http::0.0.4",
      ivy"dev.zio::zio::2.0.10"
  )

  object test extends Tests with TestModule.Munit {
    def ivyDeps = Agg(
      ivy"org.scalameta::munit::0.7.29"
    )
  }
}

