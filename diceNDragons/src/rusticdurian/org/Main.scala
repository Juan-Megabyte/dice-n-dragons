package rusticdurian.org
import rusticdurian.org.Utils._
import scala.util.Try

import zio._

import zio.http.ServerConfig.LeakDetectionLevel
import zio.http._
import zio.http.model.Method
import zio.Console._

object MainApp extends ZIOAppDefault {
  private val PORT = 9090

  private val app: HttpApp[Any, Nothing] = Http.collect[Request] {
    case Method.GET -> !! / "roll_die" =>
      val normalDie = new Die(6)
      Response.text(normalDie
                      .roll()
                      .toString)

    case req@(Method.GET -> !! / "roll_dice") if (req.url.queryParams.nonEmpty) =>
      val rolls = req.url.queryParams("rolls")(0).toInt
      val normalDie = new Die(6)
      val start = """{"rolls":[{"""
      val end = """}]}"""
      val RollsJson = normalDie
        .roll(rolls)
        .mkString(start, ",", end)
      Response.json(RollsJson)

    case req@(Method.GET -> !! / "custom_roll") if (req.url.queryParams.nonEmpty) =>
      val sides = req.url.queryParams("sides")(0).toInt
      val customDie = new Die(sides.toInt)
      Response.text(customDie
                      .roll()
                      .toString)

    case req@(Method.GET -> !! / "custom_rolls") if (req.url.queryParams.nonEmpty) =>
      val sides = req.url.queryParams("sides")(0).toInt
      val rolls = req.url.queryParams("rolls")(0).toInt
      val customDie = new Die(sides)
      val start = """{"rolls":[{"""
      val end = """}]}"""
      val RollsJson = customDie
        .roll(rolls)
        .mkString(start, ",", end)
      Response.json(RollsJson)
  }

  val run = ZIOAppArgs.getArgs.flatMap { args =>
    val nThreads: Int = args.headOption.flatMap(x => Try(x.toInt).toOption).getOrElse(0)

    val config      = ServerConfig.default
      .port(PORT)
      .leakDetection(LeakDetectionLevel.PARANOID)
      .maxThreads(nThreads)
    val configLayer = ServerConfig.live(config)

    (Server.install(app).flatMap { port =>
      Console.printLine(s"Started server on port: $port")
    } *> ZIO.never)
      .provide(configLayer, Server.live)
  }
}
