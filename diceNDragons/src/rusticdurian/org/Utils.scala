package rusticdurian.org
import scala.util.Try
import scala.util.Random
import scala.annotation.tailrec

object Utils {

  case class Die(nSides: Int):
      def roll(): Int =
        Random.between(1,nSides + 1)
      end roll

      def roll(nRolls: Int): List[Int] =
        @tailrec
        def roller(current_roll: Int = 1, results: List[Int] = List()): List[Int] =
          if current_roll >= nRolls then Random.between(1, nSides + 1) :: results
          else roller(current_roll + 1, Random.between(1, nSides + 1) :: results)
        end roller

        roller()
      end roll
  end Die
}
